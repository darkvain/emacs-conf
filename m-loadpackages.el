(load "~/.emacs.d/emacs-conf/m-packages.el")

(load-theme 'zenburn t)

(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)

(require 'evil)
(evil-mode 1)

(require 'evil-surround)
(global-evil-surround-mode 1)

(require 'evil-space)
(evil-space-mode)

(require 'evil-snipe)
(evil-snipe-mode 1)

(require 'powerline)
(require 'powerline-evil)
(powerline-evil-center-color-theme)

(require 'evil-escape) 
(evil-escape-mode)

(global-evil-tabs-mode t)

(define-key evil-normal-state-map (kbd "j") 'evil-next-visual-line)
(define-key evil-normal-state-map (kbd "k") 'evil-previous-visual-line)

(define-key evil-normal-state-map (kbd "C-k") (lambda ()
                    (interactive)
                    (evil-scroll-up nil)))
(define-key evil-normal-state-map (kbd "C-j") (lambda ()
                        (interactive)
                        (evil-scroll-down nil)))
;; OLD STUFF;

;;(require 'evil-nerd-commenter)
;;(evilnc-default-hotkeys)
;;
;;(require 'neotree)
;;(global-set-key (kbd "C-c n") 'neotree-toggle)
;;(add-hook 'neotree-mode-hook
;;            (lambda ()
;;              (define-key evil-normal-state-local-map (kbd "TAB") 'neotree-enter)
;;              (define-key evil-normal-state-local-map (kbd "SPC") 'neotree-enter)
;;              (define-key evil-normal-state-local-map (kbd "q") 'neotree-hide)
;;              (define-key evil-normal-state-local-map (kbd "RET") 'neotree-enter)))
;;
;
;;(evil-snipe-override-mode 1)
;;(evil-define-key 'visual evil-snipe-mode-map "z" 'evil-snipe-f)
;;(evil-define-key 'visual evil-snipe-mode-map "Z" 'evil-snipe-F)
;;
;;(setq evil-snipe-repeat-keys t)
;;;; or 'buffer, 'whole-visible or 'whole-buffer
;;(setq evil-snipe-scope 'visible)
;;(setq evil-snipe-repeat-scope 'whole-visible)
;;(setq evil-snipe-enable-highlight t)
;;(setq evil-snipe-enable-incremental-highlight t)

;;(require 'evil-magit)
;;(add-hook 'magit-mode-hook 'turn-off-evil-snipe-mode)
;;
;;(setq TeX-auto-save t)
;;(setq TeX-parse-self t)
;;(setq-default TeX-master nil)
;;
;;(autoload 'auto-update-file-header "header2")
;;(add-hook 'write-file-hooks 'auto-update-file-header)
;;(add-hook 'latex-mode-hook   'auto-make-header)

;;(require 'smart-tabs-mode)
;;(smart-tabs-insinuate 'c 'javascript)
;;
;;(require 'yasnippet)
;;(yas-global-mode 1)
;;
;;(require 'auto-complete-config)
;;(add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict")
;;(ac-config-default)
;;; set the trigger key so that it can work together with yasnippet on tab key,
;;; if the word exists in yasnippet, pressing tab will cause yasnippet to
;;; activate, otherwise, auto-complete will
;;(ac-set-trigger-key "TAB")
;;(ac-set-trigger-key "<tab>")

;;(require 'ac-js2)
;;(require 'js2-mode)
;;(add-hook 'js-mode-hook 'js2-minor-mode)
;;(add-hook 'js2-mode-hook 'ac-js2-mode)

