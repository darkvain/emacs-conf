(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.milkbox.net/packages/") t)
(add-to-list 'package-archives
	     '("marmalade" . "http://marmalade-repo.org/packages/") t)
(package-initialize)

; auto-install
(defvar required-packages
  '(
    zenburn-theme
;;    magit
;;    smart-tabs-mode
;;	yasnippet
;;	auto-complete
;;	js2-mode
;;	ac-js2
	powerline
	neotree
	evil
	powerline-evil
	evil-snipe
	evil-surround
	evil-space
	evil-escape
	evil-tabs
	lua-mode
;;	evil-nerd-commenter
;;	evil-magit
;;	auctex
;;	header2
    ) "auto-installed packages")

(require 'cl)

; method to check if all packages are installed
(defun packages-installed-p ()
  (loop for p in required-packages
        when (not (package-installed-p p)) do (return nil)
        finally (return t)))

; if not all packages are installed, check one by one and install the missing ones.
(unless (packages-installed-p)
  ; check for new packages (package versions)
  (message "%s" "Emacs is now refreshing its package database...")
  (package-refresh-contents)
  (message "%s" " done.")
  ; install the missing packages
  (dolist (p required-packages)
    (when (not (package-installed-p p))
      (package-install p))))
